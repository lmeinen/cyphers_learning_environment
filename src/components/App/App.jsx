// React
import React, { Suspense } from "react";
import { BrowserRouter, Route } from "react-router-dom";

// React-Bootstrap
import { Navbar, Container, Spinner } from "react-bootstrap";

// Components
import exercises from "../Assignments/exercises";
import Navigation from "../Navigation/Navigation";
import Exercise from "../Exercise/Exercise";
import Theory from "../Theory/Theory";
import About from "../About/About";

import abzLogo from "./abz-web-logo.png";

// Global constants
const homeURL = "/";
const theoryURL = "/theory";
const aboutURL = "/about";

function App() {
  return (
    <div>
      {/* Navbar should always be accessible */}
      <Navbar bg="dark" variant="dark" expand="lg">
        <Navbar.Brand href={homeURL}>
          <img src={abzLogo} height="70" alt="ABZ Informatik" />
        </Navbar.Brand>
        <Container fluid className="justify-content-end">
          <Navbar.Brand href={homeURL}>Home</Navbar.Brand>
          <Navbar.Brand href={theoryURL}>Theorie</Navbar.Brand>
          <Navbar.Brand href={aboutURL}>Über uns</Navbar.Brand>
        </Container>
      </Navbar>
      {/* Routing logic */}
      <BrowserRouter>
        <Route exact path={homeURL}>
          <Navigation exercises={exercises} />
        </Route>
        <Route exact path={theoryURL}>
          <Theory />
        </Route>
        <Route exact path={aboutURL}>
          <About />
        </Route>

        {/* This means we need to add exercises individually, which is a hassle, but we can also make them less generic, which is nice */}
        {exercises.map((ex) => {
          const Comp = ex.component;
          return (
            <Route key={ex.title} exact path={ex.path}>
              <Exercise title={ex.title}>
                <Suspense
                  fallback={
                    <Spinner animation="border" role="status">
                      <span className="sr-only">Loading...</span>
                    </Spinner>
                  }
                >
                  <Comp redirect={ex.redirect} />
                </Suspense>
              </Exercise>
            </Route>
          );
        })}
      </BrowserRouter>
    </div>
  );
}

export default App;
