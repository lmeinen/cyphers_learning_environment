import { lazy } from "react";

// -------------- Exercise Components --------------
const TranspositionExercise = lazy(() =>
  import("./TranspositionExercise/TranspositionExercise")
);

const CaesarEasy = lazy(() => import("./Caesar/CaesarEasy"));
const CaesarMedium = lazy(() => import("./Caesar/CaesarMedium"));
const CaesarHard = lazy(() => import("./Caesar/CaesarHard"));

const MaskedCaesarEasy = lazy(() => import("./MaskedCaesar/MaskedCaesarEasy"));

const SpecialistEasy = lazy(() => import("./Specialist/SpecialistEasy"));
const SpecialistMedium = lazy(() => import("./Specialist/SpecialistMedium"));
const SpecialistHard = lazy(() => import("./Specialist/SpecialistHard"));

// -------------- Exercise categories --------------
const transposition = "Transposition";
const monoAlph = "Monoalphabetische Verschlüsselung";
const polyAlph = "Polyalphabetische Verschlüsselung";

/**
 * An array that contains one object per exercise.
 * Each object must have the following fields:
 *  - title: The unique title of the exercise
 *  - difficulty: A number in range [1-5] representing the relative difficulty of the exercise
 *  - category: One of the categories listed above
 *  - path: The unique URL extension to use to navigate to the exercise
 *  - redirect: The redirect link to use in the success alert. This should usually either lead to the next exercise to solve or back to the homepage (i.e. "/")
 *  - component: The component to use to render the exercise. It should be included as a lazy import above to avoid loading all components everytime the homepage is loaded
 */
const exercises = [
  {
    title: "Caesar Verschlüsselung I",
    difficulty: 1,
    category: monoAlph,
    path: "/caesar_easy",
    redirect: "/caesar_medium",
    component: CaesarEasy,
  },
  {
    title: "Caesar Verschlüsselung II",
    difficulty: 2,
    category: monoAlph,
    path: "/caesar_medium",
    redirect: "/",
    component: CaesarMedium,
  },
  {
    title: "Caesar Verschlüsselung III",
    difficulty: 3,
    category: polyAlph,
    path: "/caesar_hard",
    redirect: "/",
    component: CaesarHard,
  },
  {
    title: "Maskiert-Caesar Verschlüsselung I",
    difficulty: 3,
    category: polyAlph,
    path: "/maskedCaesar_easy",
    redirect: "/",
    component: MaskedCaesarEasy,
  },
  {
    title: "Spezialist I",
    difficulty: 2,
    category: polyAlph,
    path: "/specialist_easy",
    redirect: "/specialist_medium",
    component: SpecialistEasy,
  },
  {
    title: "Spezialist II",
    difficulty: 3,
    category: polyAlph,
    path: "/specialist_medium",
    redirect: "/",
    component: SpecialistMedium,
  },
  {
    title: "Spezialist III",
    difficulty: 4,
    category: polyAlph,
    path: "/specialist_hard",
    redirect: "/",
    component: SpecialistHard,
  },
  {
    title: "Transposition I",
    difficulty: 1,
    category: transposition,
    path: "/transposition",
    redirect: "/",
    component: TranspositionExercise,
  },
];

export default exercises;
