import React, { useState } from "react";
import PropTypes from "prop-types";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { ListGroup, ListGroupItem } from "react-bootstrap";

import "./TranspositionExercise.css";

function TranspositionExercise() {
  const [letterList, setLetterList] = useState(["A", "B", "C", "D", "E", "F"]);
  const handleOnDragEnd = (result) => {
    if (!result.destination) {
      return;
    }
    const curr = Array.from(letterList);
    const [reorderedItem] = curr.splice(result.source.index, 1);
    curr.splice(result.destination.index, 0, reorderedItem);
    setLetterList(curr);
  };
  return (
    <DragDropContext onDragEnd={handleOnDragEnd}>
      <Droppable droppableId="Geheimtext" direction="horizontal">
        {(providedDrop) => (
          <ListGroup
            horizontal="md"
            {...providedDrop.droppableProps}
            ref={providedDrop.innerRef}
          >
            {letterList.map((v, i) => {
              return (
                <Draggable key={v} draggableId={v} index={i}>
                  {(providedDrag) => (
                    <ListGroupItem
                      ref={providedDrag.innerRef}
                      {...providedDrag.draggableProps}
                      {...providedDrag.dragHandleProps}
                    >
                      {v}
                    </ListGroupItem>
                  )}
                </Draggable>
              );
            })}
            {providedDrop.placeholder}
          </ListGroup>
        )}
      </Droppable>
    </DragDropContext>
  );
}

TranspositionExercise.propTypes = {
  redirect: PropTypes.string,
};

TranspositionExercise.defaultProps = {
  redirect: "/",
};

export default TranspositionExercise;
