import React, { useState, useRef } from "react";
import PropTypes from "prop-types";
import {
  Col,
  Container,
  Form,
  InputGroup,
  Row,
  Button,
  Overlay,
  Tooltip,
  Alert,
} from "react-bootstrap";
import { Redirect } from "react-router-dom";
import {
  encryptTextWithKey,
  decryptTextWithKey,
} from "../assignments-util/encryption-util";
import "./KeyCipherExercise.css";

// Exercise contents must expose: Exercise description and submission form
/** */
function KeyCipherExercise({
  redirect,
  redirectTxt,
  validMsg,
  invalidMsg,
  exerciseDescription,
  cipherKey,
  minKeyLength,
  maxKeyLength,
  clearTxtToShow,
  cipherTxtToShow,
  showClearTxt,
  showCipherTxt,
  initTxt,
  setAnswerTxtToShow,
  isDecryptionExercise,
  hasValidation,
  keyExpansionFunction,
}) {
  const [studentAnswer, setAnswer] = useState("");
  const [showInvalid, setShowInvalid] = useState(false);
  const [showValid, setShowValid] = useState(false);
  const [showLengthHint, setShowLengthHint] = useState(false);
  const [doRedirect, setDoRedirect] = useState(false);
  const overlayTarget = useRef();

  const retHTML = (
    <>
      <p>{exerciseDescription}</p>
      {doRedirect && <Redirect to={redirect} />}
      <Form
        inline
        noValidate
        onSubmit={handleKeySubmit(
          setShowInvalid,
          setShowValid,
          setShowLengthHint,
          minKeyLength,
          maxKeyLength,
          initTxt,
          setAnswerTxtToShow,
          isDecryptionExercise,
          studentAnswer,
          cipherKey,
          hasValidation,
          keyExpansionFunction
        )}
      >
        <Form.Label htmlFor="answerField" srOnly>
          Antwort-Eingabefeld
        </Form.Label>
        <InputGroup hasValidation className="mb-2 mr-sm-2">
          <Form.Control
            id="answerField"
            ref={overlayTarget}
            placeholder="Schlüssel"
            isInvalid={showInvalid}
            isValid={showValid}
            onChange={handleAnswerChange(
              setAnswer,
              setShowInvalid,
              setShowValid,
              setShowLengthHint
            )}
          />
          <InputGroup.Append>
            <Button type="submit" variant="dark" className="mb-2">
              Submit
            </Button>
          </InputGroup.Append>
          <Overlay
            target={overlayTarget.current}
            show={showLengthHint && studentAnswer.length > maxKeyLength}
            placement="left"
          >
            {(props) => (
              <Tooltip id="overlay-example" {...props}>
                Der Schlüssel darf höchstens Länge {maxKeyLength} haben
              </Tooltip>
            )}
          </Overlay>
          <Overlay
            target={overlayTarget.current}
            show={showLengthHint && studentAnswer.length < minKeyLength}
            placement="left"
          >
            {(props) => (
              <Tooltip id="overlay-example" {...props}>
                Der Schlüssel muss mindestens Länge {minKeyLength} haben
              </Tooltip>
            )}
          </Overlay>
        </InputGroup>
      </Form>

      {showInvalid && (
        <Alert
          variant="danger"
          onClose={() => setShowInvalid(false)}
          dismissible
        >
          <Alert.Heading>Knapp daneben!</Alert.Heading>
          <p>{invalidMsg}</p>
        </Alert>
      )}
      {showValid && (
        <Alert
          variant="success"
          onClose={() => setShowValid(false)}
          dismissible
        >
          <Alert.Heading>Richtig!</Alert.Heading>
          <p>{validMsg}</p>
          <hr />
          <Button
            variant="outline-success"
            onClick={() => {
              setDoRedirect(true);
            }}
          >
            {redirectTxt}
          </Button>
        </Alert>
      )}

      <Container>
        <Row>
          {showClearTxt && (
            <Col>
              <h3>Entschlüsselter Text:</h3>
              <p>{clearTxtToShow}</p>
            </Col>
          )}
          {showCipherTxt && (
            <Col>
              <h3>Verschüllerter Text:</h3>
              <p>{cipherTxtToShow}</p>
            </Col>
          )}
        </Row>
      </Container>
    </>
  );

  return retHTML;
}

KeyCipherExercise.propTypes = {
  redirect: PropTypes.string,
  redirectTxt: PropTypes.string,
  validMsg: PropTypes.string,
  invalidMsg: PropTypes.string,
  exerciseDescription: PropTypes.string.isRequired,
  cipherKey: PropTypes.string.isRequired,
  minKeyLength: PropTypes.number,
  maxKeyLength: PropTypes.number,
  clearTxtToShow: PropTypes.string,
  cipherTxtToShow: PropTypes.string,
  showClearTxt: PropTypes.bool,
  showCipherTxt: PropTypes.bool,
  initTxt: PropTypes.string.isRequired,
  setAnswerTxtToShow: PropTypes.func,
  isDecryptionExercise: PropTypes.bool,
  hasValidation: PropTypes.bool,
  keyExpansionFunction: PropTypes.func,
};

KeyCipherExercise.defaultProps = {
  redirect: "/",
  redirectTxt: "Zur Startseite »",
  validMsg: "Richtig! Bist du bereit für die nächste Aufgabe?",
  invalidMsg: "Versuch's doch mit einem anderen Schlüssel nochmal.",
  minKeyLength: 1,
  maxKeyLength: 4,
  clearTxtToShow:
    "Gebe einen Schlüssel ein um den resultierenden Klartext zu sehen.",
  cipherTxtToShow:
    "Gebe einen Schlüssel ein um den resultierenden Schlüsseltext zu sehen",
  showClearTxt: true,
  showCipherTxt: true,
  setAnswerTxtToShow: () => "",
  isDecryptionExercise: true,
  hasValidation: true,
  keyExpansionFunction: (k) => k,
};

// Handler functions

function handleKeySubmit(
  setShowInvalid,
  setShowValid,
  setShowLengthHint,
  minKeyLength,
  maxKeyLength,
  initTxt,
  setAnswerTxtToShow,
  isDecryptionExercise,
  studentAnswer,
  cipherKey,
  hasValidation,
  keyExpansionFunction
) {
  return (event) => {
    event.preventDefault();
    if (
      studentAnswer.length >= minKeyLength &&
      studentAnswer.length <= maxKeyLength
    ) {
      setAnswerTxtToShow(
        isDecryptionExercise
          ? decryptTextWithKey(initTxt, keyExpansionFunction(studentAnswer))
          : encryptTextWithKey(initTxt, keyExpansionFunction(studentAnswer))
      );
      if (studentAnswer === cipherKey) {
        setShowValid(hasValidation);
      } else {
        setShowInvalid(hasValidation);
      }
    } else {
      setShowLengthHint(true);
      setTimeout(() => {
        setShowLengthHint(false);
      }, 2000);
    }
  };
}

function handleAnswerChange(
  setAnswer,
  setShowInvalid,
  setShowValid,
  setShowLengthHint
) {
  return (event) => {
    event.preventDefault();
    setAnswer(event.target.value);
    setShowInvalid(false);
    setShowValid(false);
    setShowLengthHint(false);
  };
}

export default KeyCipherExercise;
