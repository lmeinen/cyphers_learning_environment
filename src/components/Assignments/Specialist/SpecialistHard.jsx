import React from "react";
import PropTypes from "prop-types";
import Specialist from "./Specialist";

// Exercise contents must expose: Exercise description and submission form
/**
 * 1. Randomly select a text from a relatively large dataset
 * 2. Randomly generate a key (length 1-5)
 * 3. Encrypt selected text using key
 * 4. Generate form with key input field
 * 5. Upon submission, decrypt using given key --> Validate key and show decrypted text
 * @returns
 */
function SpecialistHard({ redirect }) {
  const exerciseDescription =
    "Mit welchem mono- oder polyalphabetischen Schlüssel wurde der untenstehenden Text verschlüsselt? Achtung: Dieses mal ist der Klartext nicht unbedingt auf Deutsch! Vergleiche die Verteilung der relativen Häufigkeiten und die Friedmannsche Charakteristik mit die der verschiedenen Sprachen um den Schlüssel herauszufinden. Die Friedmannsche Charakteristiken findest du oben unter 'Theorie'.";
  const validMsg =
    "Zeit, dir selber auf die Schulter zu klopfen. Hiermit bist du nämlich durch! Wir haben dir die schwierigste Aufgaben die wir uns ausdenken konnten vorgesetzt und du konntest sie alle (hoffentlich problemlos) lösen. Falls du weiter üben möchtest, kannst du einfach die Seite neu laden, oder du kannst dir unter 'Über uns' anschauen, was es sonst noch alles gibt.";
  const redirectTxt = "Zur Startseite »";
  const multipleLanguages = true;
  const showInitTxt = false;
  return (
    <Specialist
      redirect={redirect}
      redirectTxt={redirectTxt}
      validMsg={validMsg}
      exerciseDescription={exerciseDescription}
      multipleLanguages={multipleLanguages}
      showInitTxt={showInitTxt}
    />
  );
}

SpecialistHard.propTypes = {
  redirect: PropTypes.string.isRequired,
};

export default SpecialistHard;
