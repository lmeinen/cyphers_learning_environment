import React, { useState } from "react";
import PropTypes from "prop-types";
import KeyCipherExercise from "../KeyCipherExercise/KeyCipherExercise";
import Statistics from "../../Statistics/Statistics";
import { randomKey, randomProperty } from "../assignments-util/helpers";
import { textsDE, textsFR, textsEN } from "../sample_texts/texts";
import { encryptTextWithKey } from "../assignments-util/encryption-util";

// Exercise contents must expose: Exercise description and submission form
/**
 * 1. Randomly select a text from a relatively large dataset
 * 2. Randomly generate a key (length 1-5)
 * 3. Encrypt selected text using key
 * 4. Generate form with key input field
 * 5. Upon submission, decrypt using given key --> Validate key and show decrypted text
 * @returns
 */
function Specialist({
  redirect,
  redirectTxt,
  validMsg,
  invalidMsg,
  exerciseDescription,
  multipleLanguages,
  showInitTxt,
}) {
  const textsToChooseFrom = multipleLanguages
    ? randomProperty([textsDE, textsFR, textsEN])
    : textsDE;
  const [initTxt] = useState(
    randomProperty(textsToChooseFrom).replace(/\s{2,}/g, " ")
  );
  const [key] = useState(randomKey(1 + Math.round(Math.random() * 3)));
  const [clearTxtToShow, setClearTxtToShow] = useState(
    showInitTxt
      ? initTxt
      : "Gebe einen Schlüssel ein und drücke auf 'Submit' um den entschlüsselten Text zu sehen!"
  );
  const [cipherTxtToShow] = useState(encryptTextWithKey(initTxt, key));
  return (
    <>
      <Statistics
        inputTxt={initTxt}
        maxKeyLength={4}
        showFCGraph
        languagesToShow={multipleLanguages ? ["DE", "FR", "EN"] : ["DE"]}
      />
      <KeyCipherExercise
        redirect={redirect}
        redirectTxt={redirectTxt}
        validMsg={validMsg}
        invalidMsg={invalidMsg}
        exerciseDescription={exerciseDescription}
        cipherKey={key}
        minKeyLength={1}
        maxKeyLength={4}
        clearTxtToShow={clearTxtToShow}
        cipherTxtToShow={cipherTxtToShow}
        showClearTxt
        showCipherTxt
        initTxt={cipherTxtToShow}
        setAnswerTxtToShow={setClearTxtToShow}
        isDecryptionExercise
        hasValidation={false}
      />
    </>
  );
}

Specialist.propTypes = {
  redirect: PropTypes.string,
  redirectTxt: PropTypes.string,
  validMsg: PropTypes.string,
  invalidMsg: PropTypes.string,
  exerciseDescription: PropTypes.string.isRequired,
  multipleLanguages: PropTypes.bool,
  showInitTxt: PropTypes.bool,
};

// defaults simply correspond to easy variant
Specialist.defaultProps = {
  redirect: "/",
  redirectTxt: "Zur Startseite »",
  validMsg: "Richtig! Bist du bereit für die nächste Aufgabe?",
  invalidMsg: "Versuch's doch mit einem anderen Schlüssel nochmal.",
  multipleLanguages: false,
  showInitTxt: true,
};

export default Specialist;
