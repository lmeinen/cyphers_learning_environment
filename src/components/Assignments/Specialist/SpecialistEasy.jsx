import React from "react";
import PropTypes from "prop-types";
import Specialist from "./Specialist";

// Exercise contents must expose: Exercise description and submission form
/**
 * 1. Randomly select a text from a relatively large dataset
 * 2. Randomly generate a key (length 1-5)
 * 3. Encrypt selected text using key
 * 4. Generate form with key input field
 * 5. Upon submission, decrypt using given key --> Validate key and show decrypted text
 * @returns
 */
function SpecialistEasy({ redirect }) {
  const exerciseDescription =
    "Mit welchem mono- oder polyalphabetischen Schlüssel wurde der untenstehenden Text verschlüsselt?";
  const validMsg =
    "Gar nicht schlecht der Specht! Aber bist du bereit für das nächste Level?";
  const redirectTxt = "Zur nächsten Aufgabe »";
  const multipleLanguages = false;
  const showInitTxt = true;
  return (
    <Specialist
      redirect={redirect}
      redirectTxt={redirectTxt}
      validMsg={validMsg}
      exerciseDescription={exerciseDescription}
      multipleLanguages={multipleLanguages}
      showInitTxt={showInitTxt}
    />
  );
}

SpecialistEasy.propTypes = {
  redirect: PropTypes.string.isRequired,
};

export default SpecialistEasy;
