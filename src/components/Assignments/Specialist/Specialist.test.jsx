import React from "react";
import { render } from "@testing-library/react";
import Specialist from "./Specialist";
import SpecialistEasy from "./SpecialistEasy";
import SpecialistMedium from "./SpecialistMedium";
import SpecialistHard from "./SpecialistHard";

describe("Specialist components", () => {
  test("Specialist variants render normally", () => {
    render(<SpecialistEasy redirect="/" />);
    render(<SpecialistMedium redirect="/" />);
    render(<SpecialistHard redirect="/" />);
  });
  test("Specialist component edge cases render normally", () => {
    render(
      <Specialist
        redirect=""
        redirectTxt=""
        validMsg=""
        exerciseDescription=""
        multipleLanguages
        showInitTxt
      />
    );
    render(
      <Specialist
        redirect=""
        redirectTxt=""
        validMsg=""
        exerciseDescription=""
        multipleLanguages={false}
        showInitTxt={false}
      />
    );
  });
});
