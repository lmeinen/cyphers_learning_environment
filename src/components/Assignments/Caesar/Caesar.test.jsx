import React from "react";
import { render } from "@testing-library/react";
import CaesarEasy from "./CaesarEasy";
import CaesarMedium from "./CaesarMedium";
import CaesarHard from "./CaesarHard";

describe("Caesar components", () => {
  test("Caesar variants should render normally", () => {
    render(<CaesarEasy redirect="/" />);
    render(<CaesarMedium redirect="/" />);
    render(<CaesarHard redirect="/" />);
  });
});
