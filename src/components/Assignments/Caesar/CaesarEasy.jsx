import React, { useState } from "react";
import PropTypes from "prop-types";
import KeyCipherExercise from "../KeyCipherExercise/KeyCipherExercise";
import Statistics from "../../Statistics/Statistics";
import { randomKey, randomProperty } from "../assignments-util/helpers";
import { encryptTextWithKey } from "../assignments-util/encryption-util";
import { textsDE } from "../sample_texts/texts";

function CaesarEasy({ redirect }) {
  const exerciseDescription =
    "Der römische Kaiser hat eine neue Chiffriermethode entwickelt. Voller Stollz zeigt er dir ein Beispiel. Schaffst du's den Schlüssel zu finden, der für diese Caesar-Verschlüsselung verwendet wurde? Zur Unterstützung haben wir dir oben die Verteilung der relativen Häufigkeiten eingezeichnet.";
  const validMsg =
    "So leicht lässt sich der grosse Caesar aber nicht besiegen! Nun mal schauen ob du's auch ohne Klartext schaffst.";
  const redirectTxt = "Zur nächsten Aufgabe »";
  const initTxt = randomProperty(textsDE).replace(/\s{2,}/g, " ");
  const [key] = useState(randomKey(1));
  const [clearTxtToShow, setClearTxtToShow] = useState(initTxt);
  const [cipherTxt] = useState(encryptTextWithKey(initTxt, key));
  return (
    <>
      <Statistics inputTxt={cipherTxt} />
      <KeyCipherExercise
        redirect={redirect}
        redirectTxt={redirectTxt}
        validMsg={validMsg}
        exerciseDescription={exerciseDescription}
        cipherKey={key}
        minKeyLength={1}
        maxKeyLength={1}
        clearTxtToShow={clearTxtToShow}
        cipherTxtToShow={cipherTxt}
        initTxt={cipherTxt}
        setAnswerTxtToShow={setClearTxtToShow}
      />
    </>
  );
}

CaesarEasy.propTypes = {
  redirect: PropTypes.string,
};

CaesarEasy.defaultProps = {
  redirect: "/",
};

export default CaesarEasy;
