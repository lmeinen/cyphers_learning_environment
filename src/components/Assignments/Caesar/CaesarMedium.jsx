import React, { useState } from "react";
import PropTypes from "prop-types";
import KeyCipherExercise from "../KeyCipherExercise/KeyCipherExercise";
import Statistics from "../../Statistics/Statistics";
import { randomKey, randomProperty } from "../assignments-util/helpers";
import { encryptTextWithKey } from "../assignments-util/encryption-util";
import { textsDE } from "../sample_texts/texts";

function CaesarMedium({ redirect }) {
  const exerciseDescription =
    "Jemand hat mithilfe der Caesar-Verschlüsselung eine Geheimnachricht verschlüsselt. Schaffst du's, die verborgene Weisheiten aufzudecken? Benutze dazu die oben eingezeichneten relativen Häufigkeitsverteilungen.";
  const redirectTxt = "Zur nächsten Aufgabe »";
  const validMsg =
    "Tolle Sache! Schaffst du den letzten Schritt um Caesars Verschlüsselungsmethode endgültig zu besiegen?";
  const initTxt = randomProperty(textsDE).replace(/\s{2,}/g, " ");
  const [key] = useState(randomKey(1));
  const [clearTxtToShow, setClearTxtToShow] = useState("");
  const [cipherTxt] = useState(encryptTextWithKey(initTxt, key));
  return (
    <>
      <Statistics inputTxt={cipherTxt} />
      <KeyCipherExercise
        redirect={redirect}
        redirectTxt={redirectTxt}
        validMsg={validMsg}
        exerciseDescription={exerciseDescription}
        cipherKey={key}
        minKeyLength={1}
        maxKeyLength={1}
        clearTxtToShow={clearTxtToShow}
        cipherTxtToShow={cipherTxt}
        initTxt={cipherTxt}
        setAnswerTxtToShow={setClearTxtToShow}
      />
    </>
  );
}

CaesarMedium.propTypes = {
  redirect: PropTypes.string,
};

CaesarMedium.defaultProps = {
  redirect: "/",
};

export default CaesarMedium;
