import React, { useState } from "react";
import PropTypes from "prop-types";
import KeyCipherExercise from "../KeyCipherExercise/KeyCipherExercise";
import Statistics from "../../Statistics/Statistics";
import { randomKey, randomProperty } from "../assignments-util/helpers";
import { encryptTextWithKey } from "../assignments-util/encryption-util";
import { textsDE } from "../sample_texts/texts";

function CaesarHard({ redirect }) {
  const exerciseDescription =
    "Der römische Kaiser hat sich eine spezielle Verallgemeinerung ausgedacht: Alle Buchstaben auf ungeraden Positionen wurden mit einem Schlüssel i caesar-verschlüsselt, alle auf geraden Positionen mit einem Schlüssel j. Mit welchem Schlüssel der Länge 2 wurde der untenstehenden Text verschlüsselt?";
  const validMsg =
    "Du hast die CAESAR-Verschlüsselung nun endgültig besiegt. Gönne dir die wohlverdiente Pause und dann auf zum nächsten Abenteuer!";
  const initTxt = randomProperty(textsDE).replace(/\s{2,}/g, " ");
  const [key] = useState(randomKey(2));
  const [clearTxtToShow, setClearTxtToShow] = useState(
    "Gebe einen Schlüssel ein um den resultierenden Klartext zu sehen."
  );
  const [cipherTxt] = useState(encryptTextWithKey(initTxt, key));
  return (
    <>
      <Statistics inputTxt={cipherTxt} keyLen={2} />
      <KeyCipherExercise
        redirect={redirect}
        validMsg={validMsg}
        exerciseDescription={exerciseDescription}
        cipherKey={key}
        minKeyLength={2}
        maxKeyLength={2}
        clearTxtToShow={clearTxtToShow}
        cipherTxtToShow={cipherTxt}
        initTxt={cipherTxt}
        setAnswerTxtToShow={setClearTxtToShow}
      />
    </>
  );
}

CaesarHard.propTypes = {
  redirect: PropTypes.string,
};

CaesarHard.defaultProps = {
  redirect: "/",
};

export default CaesarHard;
