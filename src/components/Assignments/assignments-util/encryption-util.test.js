import * as Util from "./encryption-util";

describe("Decrypt char", () => {
  test("Non-edge cases should work normally", () => {
    expect(Util.getClearTextChar("a", "a")).toBe("a");
    expect(Util.getClearTextChar("o", "k")).toBe("e");
    expect(Util.getClearTextChar("o", "e")).toBe("k");
    expect(Util.getClearTextChar("o", "y")).toBe("q");
  });
  test("Case should be preserved", () => {
    expect(Util.getClearTextChar("O", "k")).toBe("E");
    expect(Util.getClearTextChar("O", "e")).toBe("K");
    expect(Util.getClearTextChar("O", "y")).toBe("Q");
  });
  test.todo("Implement edge-case tests");
});
describe("Encrypt char", () => {
  test("Non-edge cases should work normally", () => {
    expect(Util.getCipherTextChar("a", "a")).toBe("a");
    expect(Util.getCipherTextChar("e", "k")).toBe("o");
    expect(Util.getCipherTextChar("k", "e")).toBe("o");
    expect(Util.getCipherTextChar("q", "y")).toBe("o");
  });
  test("Case should be preserved", () => {
    expect(Util.getCipherTextChar("E", "k")).toBe("O");
    expect(Util.getCipherTextChar("Q", "y")).toBe("O");
    expect(Util.getCipherTextChar("K", "e")).toBe("O");
  });
  test.todo("Implement edge-case tests");
});

describe("Encrypt text", () => {
  test("Non-edge cases should work normally", () => {
    expect(Util.encryptTextWithKey("a", "a")).toBe("a");
    expect(Util.encryptTextWithKey("abc", "a")).toBe("abc");
    expect(Util.encryptTextWithKey("abc", "abc")).toBe("ace");
    expect(Util.encryptTextWithKey("abc", "key")).toBe("kfa");
    expect(Util.encryptTextWithKey(" e", "l")).toBe(" p");
  });
  test.todo("Empty text should always return normally");
  test.todo("Empty key should throw an error");
  test.todo("Case should be preserved");
});

describe("Decrypt text", () => {
  test("Non-edge cases should work normally", () => {
    expect(Util.decryptTextWithKey("a", "a")).toBe("a");
    expect(Util.decryptTextWithKey("abc", "a")).toBe("abc");
    expect(Util.decryptTextWithKey("abc", "abc")).toBe("aaa");
    expect(Util.decryptTextWithKey("abc", "key")).toBe("qxe");
  });
  test.todo("Empty should always return normally");
  test.todo("Empty key should throw an error");
  test.todo("Case should be preserved");
});

describe("Both directions together", () => {
  test("Encryption then decryption", () => {
    expect(
      Util.decryptTextWithKey(Util.encryptTextWithKey("hello", "key"), "key")
    ).toBe("hello");
  });
  test.todo("Decryption then encryption");
});
