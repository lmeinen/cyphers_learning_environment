import { getAlphabet } from "../../Statistics/statistics-util/statistics-util";

export function randomProperty(obj) {
  const keys = Object.keys(obj);
  return obj[keys[(keys.length * Math.random()) << 0]];
}

export function randomKey(length) {
  let str = "";
  const alph = getAlphabet();
  for (let i = 0; i < length; i += 1) {
    str += alph[Math.round(Math.random() * alph.length)];
  }
  return str;
}
