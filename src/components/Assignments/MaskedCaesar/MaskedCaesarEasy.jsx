import React, { useState } from "react";
import PropTypes from "prop-types";
import KeyCipherExercise from "../KeyCipherExercise/KeyCipherExercise";
import Statistics from "../../Statistics/Statistics";
import { randomKey, randomProperty } from "../assignments-util/helpers";
import { encryptTextWithKey } from "../assignments-util/encryption-util";
import { textsDE } from "../sample_texts/texts";

function MaskiertCaesarEasy({ redirect }) {
  const len = 2 + Math.round(Math.random() * 3); // length of bitstring
  const bitString = [];
  for (let i = 0; i < len; i += 1) {
    bitString.push(Math.round(Math.random()));
  }
  const [bitStringStored] = useState(bitString);
  const bitStringExpansion = (k) => {
    let expandedKey = "";
    bitStringStored.forEach((b) => {
      expandedKey += b === 1 ? k : "a";
    });
    return expandedKey;
  };

  const exerciseDescription = `Man hat sich ein neues Kryptosystem MASKIERT-CAESAR überlegt. Das Kryptosystem hat als Schlüssel ein Paar (Bitfolge, j). Zuerst wird die Bitfolge unter einem Klartext wiederholt hingeschrieben, so dass unter jedem Buchstaben des Klartextes ein Bit steht. Die Bitfolge verwendet man als eine Maske. Wenn eine 0 unter einem Buchstaben steht, wird der Buchstabe einfach in den Geheimtext kopiert. Falls dort eine 1 steht, wird der Buchstabe mit dem Schlüssel j chiffriert. Angenommen die Maskierung ${bitString.join(
    ""
  )} wurde verwendet, entschlüssle den nachfolgenden Text.`;
  const redirectTxt = "Weiter zur nächsten Aufgabe »";
  const validMsg =
    "Kein Kryptograph ist dir gewappnet! Bist du bereit für die nächste Schwierigkeitsstufe?";
  const initTxt = randomProperty(textsDE).replace(/\s{2,}/g, " ");
  const [key] = useState(randomKey(1));
  const [clearTextToShow, setClearTextToShow] = useState(
    "Gebe einen Schlüssel ein um den resultierenden Klartext zu sehen."
  );
  const [cipherTxt] = useState(
    encryptTextWithKey(initTxt, bitStringExpansion(key))
  );
  return (
    <>
      <Statistics inputTxt={cipherTxt} keyLen={len} />
      <KeyCipherExercise
        redirect={redirect}
        redirectTxt={redirectTxt}
        validMsg={validMsg}
        exerciseDescription={exerciseDescription}
        cipherKey={key}
        minKeyLength={1}
        maxKeyLength={1}
        clearTxtToShow={clearTextToShow}
        cipherTxtToShow={cipherTxt}
        initTxt={cipherTxt}
        setAnswerTxtToShow={setClearTextToShow}
        keyExpansionFunction={bitStringExpansion}
      />
    </>
  );
}

MaskiertCaesarEasy.propTypes = {
  redirect: PropTypes.string,
};

MaskiertCaesarEasy.defaultProps = {
  redirect: "/",
};

export default MaskiertCaesarEasy;
