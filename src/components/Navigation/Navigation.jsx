import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import Table from "react-bootstrap/Table";

export default function Navigation({ exercises }) {
  return (
    <nav>
      <Table bordered hover size="sm" variant="dark">
        <thead>
          <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Schwierigkeit</th>
          </tr>
        </thead>
        <tbody>
          {exercises.map((exercise) => (
            <tr key={exercise.title}>
              <td>
                <Link to={exercise.path}>{exercise.title}</Link>
              </td>
              <td>{exercise.category}</td>
              <td>{exercise.difficulty}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </nav>
  );
}

Navigation.propTypes = {
  exercises: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
        PropTypes.elementType,
      ])
    )
  ).isRequired,
};
