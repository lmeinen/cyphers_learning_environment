import React from "react";
import PropTypes from "prop-types";
import { Container, Breadcrumb, BreadcrumbItem } from "react-bootstrap";

import exercises from "../Assignments/exercises";

import "./Exercise.css";

function Exercise({ title, children }) {
  const metadata = exercises.find((e) => e.title === title);
  return (
    <div className="exercise">
      <Container fluid="lg">
        <Breadcrumb>
          <BreadcrumbItem href="/">Home</BreadcrumbItem>
          <BreadcrumbItem active>{metadata.category}</BreadcrumbItem>
          <BreadcrumbItem active>{metadata.title}</BreadcrumbItem>
        </Breadcrumb>
        {children}
      </Container>
    </div>
  );
}

Exercise.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
};

export default Exercise;
