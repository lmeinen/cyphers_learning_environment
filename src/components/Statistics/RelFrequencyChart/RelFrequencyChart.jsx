import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  BarChart,
  Bar,
  ReferenceLine,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

import "./RelFrequencyChart.css";

/**
 * Component to render a brush barchart displaying the relative frequencies of each letter using the provided keys as data keys.
 * The statTextRelFrequencies should look like [{ letter: "a", RH: 0.x, DE: 0.x, FR: 0.x, EN: 0.x }, ...] and keys is an
 * array of fields to consider (prolly generated based on user selection).
 */
function RelFrequencyChart({
  statTextRelFrequencies,
  keys,
  colours,
  coloursDimmed,
}) {
  const averageRelFreq =
    statTextRelFrequencies.reduce((val, elem) => val + elem.freq, 0.0) /
    statTextRelFrequencies.length;

  const [barToggle, setBarToggle] = useState(
    keys.reduce((o, key) => ({ ...o, [key]: true }), {})
  );

  React.useEffect(() => {
    keys.forEach((k) => {
      setBarToggle((b) => {
        if (b[k] === undefined && k === "Rel. Häufigkeit G0") {
          return { ...b, [k]: true };
        }
        return b;
      });
    });
  }, [keys]);

  return (
    <ResponsiveContainer width="100%" height={300}>
      <BarChart
        width={300}
        height={400}
        data={statTextRelFrequencies}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="letter" />
        <YAxis type="number" domain={[0.0, 0.2]} scale="linear" tickCount={5} />
        <Tooltip
          labelFormatter={(l) => (
            <p className="labelP">
              Index {l.charCodeAt() - "a".charCodeAt()} im Alphabet
            </p>
          )}
          formatter={(value) => value.toFixed(2)}
        />
        <Legend
          verticalAlign="top"
          align="right"
          iconType="square"
          wrapperStyle={{ lineHeight: "40px" }}
          payload={keys.map((key) => {
            return {
              value: key,
              type: "square",
              color: barToggle[key] ? colours[key] : coloursDimmed[key],
            };
          })}
          onClick={onLegendClick(barToggle, setBarToggle)}
        />
        <ReferenceLine y={averageRelFreq} stroke="grey" />
        {keys.map((key) => {
          return (
            barToggle[key] && (
              <Bar key={key} dataKey={key} name={key} fill={colours[key]} />
            )
          );
        })}
      </BarChart>
    </ResponsiveContainer>
  );
}

// TODO: Add custom validator for statTextRelFrequencies to check it only contains valid field names
RelFrequencyChart.propTypes = {
  statTextRelFrequencies: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ).isRequired,
  keys: PropTypes.arrayOf(PropTypes.string).isRequired,
  colours: PropTypes.objectOf(PropTypes.string).isRequired,
  coloursDimmed: PropTypes.objectOf(PropTypes.string).isRequired,
};

// Handler functions
function onLegendClick(barToggle, setBarToggle) {
  return (legendItem) => {
    setBarToggle({
      ...barToggle,
      [legendItem.value]: !barToggle[legendItem.value],
    });
  };
}

export default RelFrequencyChart;
