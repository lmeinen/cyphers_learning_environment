import React from "react";
import { render } from "@testing-library/react";
import RelFrequencyChart from "./RelFrequencyChart";

describe("RelFrequencyChart component", () => {
  test("Basic RelFrequencyChart render", () => {
    render(
      <RelFrequencyChart
        statTextRelFrequencies={[{ letter: "a", freq: 0.1 }]}
        keys={["freq"]}
        colours={{ freq: "#FFFFFF" }}
        coloursDimmed={{ freq: "#000000" }}
      />
    );
  });
  test("shouldn't crash for empty or invalid strings", () => {
    render(
      <RelFrequencyChart
        statTextRelFrequencies={[{ letter: "", freq: 0.0 }]}
        keys={["freq"]}
        colours={{ freq: "" }}
        coloursDimmed={{ freq: "" }}
      />
    );
    render(
      <RelFrequencyChart
        statTextRelFrequencies={[{ letter: "a", freq: 0.0 }]}
        keys={[]}
        colours={{ freq: "#FFFFFF" }}
        coloursDimmed={{ freq: "#000000" }}
      />
    );
    render(
      <RelFrequencyChart
        statTextRelFrequencies={[{ letter: "a", freq: 0.0 }]}
        keys={["frequency"]}
        colours={{ freq: "#FFFFFF" }}
        coloursDimmed={{ freq: "#000000" }}
      />
    );
  });
});
