import React, { useState, useRef } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Form,
  InputGroup,
  Row,
  Col,
  Overlay,
  Tooltip,
} from "react-bootstrap";

import {
  mergeByProperty,
  getLangStatsWithRenamedFields,
  getRelFreqWithRenamedFields,
} from "./statistics-util/helpers";

import RelFrequencyChart from "./RelFrequencyChart/RelFrequencyChart";
import FCChart from "./FCChart/FCChart";

import "./Statistics.css";

/**
 * This component offers an interface that (as a first iteration) offers:
 * - An input field with a limit of 1024 characters
 * - A submit button to indicate that statistics should be calculated based on the input field
 * - Subtle indicators directly underneath the input field showing number of characters entered and remaining
 * - A BrushBarChart from the recharts module showing: relative frequency of each letter in text, expected
 * relative frequency in different languages, a red line indicating the average relative frequency
 * - Options to en-/disable statistics in the above barchart
 * - An input field and submit button to specify keyLength and compute the FC of the given input text
 *
 * Possible additional features include:
 * - Options to switch from single letters to bi- or trigrams
 * - Options to switch from/to alphabetical or magnitude (default) ordering
 * - An info pop-up (what's the name?) for the FC showing info on language-specific values
 * - The component could accept props specifying exactly which features should be available
 *
 * Props could include:
 * - Length of letter sequences to compute rel freq for (singles, bi-, trigrams)
 * - Number of computed sequences to show (e.g. just top 3 instead of entire alphabet)
 * - Whether or not to show fc information
 * @returns Statistics component to render within an assignment
 */
function Statistics({
  inputTxt,
  keyLen,
  minKeyLength,
  maxKeyLength,
  showFCGraph,
  languagesToShow = ["DE"],
}) {
  // Local state variables - Set using React's State Hooks
  const [inputText, setInputText] = useState("");
  const [keyLength, setKeyLength] = useState(keyLen);
  const [statText, setStatText] = useState(inputTxt);
  const [relFreqs, setRelFreqs] = useState(
    languagesToShow.reduce(
      (currRelFreqs, language) =>
        mergeByProperty(getLangStatsWithRenamedFields(language), currRelFreqs),
      getRelFreqWithRenamedFields(inputTxt, keyLen === 0 ? 1 : keyLen)
    )
  );
  const [validated, setValidated] = useState(false);

  const [displayError, setDisplayError] = useState(false);
  const overlayTarget = useRef();

  const maxInputLength = 1024;
  const colours = [
    "#E42256",
    "#FEC84D",
    "#00B1B0",
    "#FF8370",
    "#BCFF70",
    "#FF00FF",
    "#B8A1FF",
    "#ECCEB0",
  ];
  const coloursDimmed = [
    "#f7bbcb",
    "#ffe7b3",
    "#b3ffff",
    "#ffbdb3",
    "#dbffb3",
    "#ffb3ff",
    "#e3d9ff",
    "#f7ebdf",
  ];

  const keysToRender = Object.keys(relFreqs[0]).filter((el) => el !== "letter");
  const coloursToRender = keysToRender.reduce(
    (o, key, index) => ({ ...o, [key]: colours[index] }),
    {}
  );
  const coloursDimmedToRender = keysToRender.reduce(
    (o, key, index) => ({ ...o, [key]: coloursDimmed[index] }),
    {}
  );

  return (
    <>
      {/* Text-to-be-analyzed submission form */}
      {inputTxt === "" && (
        <Form
          noValidate
          validated={validated}
          onSubmit={handleTextSubmit(
            inputText,
            relFreqs,
            keyLength,
            setStatText,
            setValidated,
            setRelFreqs
          )}
        >
          <Form.Group controlId="textEingabeFeld">
            <Form.Label srOnly>Eingabetext</Form.Label>
            <InputGroup hasValidation>
              <Form.Control
                required
                as="textarea"
                maxLength={maxInputLength}
                placeholder="Bitte gebe hier deinen Text ein."
                onChange={handleTextChange(setInputText, setValidated)}
              />
              <InputGroup.Append>
                <Button size="sm" variant="dark" type="submit">
                  Submit
                </Button>
              </InputGroup.Append>
              <Form.Control.Feedback type="invalid" tooltip>
                Bitte gebe Text ein.
              </Form.Control.Feedback>
            </InputGroup>
            <Form.Text id="textHelperBlock" muted>
              <span className="helptext">
                Der Text darf 1-{maxInputLength} Zeichen lang sein. Bitte
                beachte, dass ä, ö, ü und Satzzeichen in der Statistik nicht
                berücksichtigt werden.
              </span>
              <span className="charRemaining">
                {inputText.length}/{maxInputLength}
              </span>
            </Form.Text>
          </Form.Group>
        </Form>
      )}

      <RelFrequencyChart
        statTextRelFrequencies={relFreqs}
        keys={keysToRender}
        colours={coloursToRender}
        coloursDimmed={coloursDimmedToRender}
      />

      {showFCGraph && (
        <Form>
          <Form.Group as={Row} controlId="fc">
            <Form.Label column srOnly>
              Schlüssellänge
            </Form.Label>
            <Col>
              {/* Known limitation of React: No onChange or onInput event is triggered when pasting values or using the arrow keys --> It's a bit fiddly to update the graph */}
              <Form.Control
                size="md"
                type="number"
                min={minKeyLength}
                max={maxKeyLength}
                ref={overlayTarget}
                placeholder="Schlüssellänge"
                onInput={handleKeyLengthChange(
                  keyLength,
                  relFreqs,
                  statText,
                  setKeyLength,
                  setRelFreqs,
                  setDisplayError
                )}
              />
              <Overlay
                target={overlayTarget.current}
                show={displayError}
                placement="left"
              >
                {(props) => (
                  <Tooltip id="overlay-example" {...props}>
                    Bitte gebe eine Zahl zwischen 1 und 5 ein
                  </Tooltip>
                )}
              </Overlay>
            </Col>
            <Col>
              <FCChart
                statText={statText}
                keyLength={keyLength}
                colours={colours}
                showLanguageTips
              />
            </Col>
          </Form.Group>
        </Form>
      )}
      <hr className="solid" />
    </>
  );
}

Statistics.propTypes = {
  inputTxt: PropTypes.string,
  keyLen: PropTypes.number,
  minKeyLength: PropTypes.number,
  maxKeyLength: PropTypes.number,
  showFCGraph: PropTypes.bool,
  languagesToShow: PropTypes.arrayOf(PropTypes.string),
};

Statistics.defaultProps = {
  inputTxt: "",
  keyLen: 0,
  minKeyLength: 1,
  maxKeyLength: 5,
  showFCGraph: false,
  languagesToShow: ["DE"],
};

// Submission handlers for readability
function handleTextChange(setInputText, setValidated) {
  return (event) => {
    setInputText(event.target.value);
    setValidated(false);
    event.preventDefault();
  };
}

function handleTextSubmit(
  inputText,
  relFreqs,
  keyLength,
  setStatText,
  setValidated,
  setRelFreqs
) {
  return (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      setStatText(inputText);
      setRelFreqs(
        mergeByProperty(
          relFreqs,
          getRelFreqWithRenamedFields(inputText, keyLength)
        )
      );
    }
    setValidated(true);
    event.preventDefault();
  };
}

function handleKeyLengthChange(
  keyLength,
  relFreqs,
  statTxt,
  setKeyLength,
  setRelFreqs,
  setDisplayError
) {
  return (event) => {
    const form = event.currentTarget;
    if (form.checkValidity()) {
      setDisplayError(false);
      setRelFreqs(
        mergeByProperty(
          relFreqs,
          getRelFreqWithRenamedFields(statTxt, event.target.value)
        )
      );
      setKeyLength(event.target.value);
    } else {
      setDisplayError(true);
    }
  };
}

export default Statistics;
