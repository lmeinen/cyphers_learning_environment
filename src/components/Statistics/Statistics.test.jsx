import React from "react";
import { render } from "@testing-library/react";
import Statistics from "./Statistics";

describe("Statistics component", () => {
  test("Basic Statistics render", () => {
    render(<Statistics showFCGraph languagesToShow={["DE"]} />);
    render(
      <Statistics showFCGraph={false} languagesToShow={["DE", "FR", "EN"]} />
    );
  });
  test("Edge case rendering Statistics", () => {
    render(<Statistics showFCGraph={false} languagesToShow={["DE", "IT"]} />);
    render(<Statistics showFCGraph languagesToShow={["DE", "IT"]} />);
  });
});
