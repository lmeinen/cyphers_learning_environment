import * as Util from "./statistics-util";

describe("Absolute Frequencies", () => {
  test("Non-edge cases should work normally", () => {
    expect(Util.absoluteFrequency("a", "aa")).toBe(2);
    expect(Util.absoluteFrequency("a", "bb")).toBe(0);
  });
  test("Should be case-insensitive", () => {
    expect(Util.absoluteFrequency("a", "Aa")).toBe(2);
    expect(Util.absoluteFrequency("A", "aa")).toBe(2);
  });
  test("Empty input texts should always return 0", () => {
    expect(Util.absoluteFrequency("a", "")).toBe(0);
    expect(Util.absoluteFrequency("", "")).toBe(0);
  });
  test("Invalid input should throw errors", () => {
    expect(() => {
      Util.absoluteFrequency("1", "12");
    }).toThrow();
    expect(() => {
      Util.absoluteFrequency("ö", "öö");
    }).toThrow();
    expect(() => {
      Util.absoluteFrequency("", "a");
    }).toThrow();
  });
});

describe("Relative Frequencies", () => {
  test("Non-edge cases should work normally", () => {
    expect(Util.relativeFrequency("a", "aa")).toBeCloseTo(1.0);
    expect(Util.relativeFrequency("a", "a.b")).toBeCloseTo(0.5);
    expect(Util.relativeFrequency("a", "bb")).toBeCloseTo(0.0);
  });
  test("Should be case-insensitive", () => {
    expect(Util.relativeFrequency("a", "Aa")).toBeCloseTo(1.0);
    expect(Util.relativeFrequency("A", "aa")).toBeCloseTo(1.0);
  });
  test("Empty input texts should always return 0", () => {
    expect(Util.relativeFrequency("a", "")).toBeCloseTo(0.0);
    expect(Util.relativeFrequency("", "")).toBeCloseTo(0.0);
  });
  test("Invalid input should throw errors", () => {
    expect(() => {
      Util.relativeFrequency("1", "12");
    }).toThrow();
    expect(() => {
      Util.relativeFrequency("ö", "öö");
    }).toThrow();
    expect(() => {
      Util.relativeFrequency("", "a");
    }).toThrow();
  });
});

describe("Relative Frequency distribution for keylength", () => {
  test("Should work for empty string", () => {
    expect(Util.relativeFrequencyAll("", 1)[0].freq_0).toBeCloseTo(0.0);
  });
  test("Should work for non-positive keyLength", () => {
    expect(Util.relativeFrequencyAll("a", -1)[0].freq_0).toBeCloseTo(1.0);
  });
});

describe("expected relative frequency of a cipherchar for key and language", () => {
  test("Non-edge cases should work normally", () => {
    expect(Util.expectedRelativeFrequency("o", "key", "DE")).toBeCloseTo(0.064);
    expect(Util.expectedRelativeFrequency("o", "a", "DE")).toBeCloseTo(0.027);
  });
  test("Empty input texts should always return 0", () => {
    expect(Util.expectedRelativeFrequency("o", "", "DE")).toBeCloseTo(0.0);
    expect(Util.expectedRelativeFrequency("", "", "")).toBeCloseTo(0.0);
  });
  test("Invalid input should throw an error", () => {
    expect(() => {
      Util.expectedRelativeFrequency("", "a", "DE");
    }).toThrow();
    expect(() => {
      Util.expectedRelativeFrequency("a", "a", "");
    }).toThrow();
  });
});

describe("expected relative frequency of a char for language", () => {
  test("Should work normally for DE", () => {
    expect(
      Util.statRelativeFrequency("DE").find((val) => val.letter === "a").freq
    ).toBe(0.061089494308);
  });
  test.todo("Should work normally for FR");
  test.todo("Should work normally for EN");
  test.todo("Should work normally for default");
  test("Should throw error for invalid language", () => {
    expect(() => {
      Util.statRelativeFrequency("IT");
    }).toThrow();
  });
});

describe("Friedman's Characteristic", () => {
  test("Non-edge cases for given text should work normally", () => {
    expect(
      Util.friedmansCharacteristic("abcdefghijklmnopqrstuvwxyz")
    ).toBeCloseTo(0.0);
    expect(Util.friedmansCharacteristic("aaaaaaaaaaaaaaaaaaaa")).toBeCloseTo(
      0.96,
      1
    );
  });
  test("Non-edge cases for given text and keylength should work normally", () => {
    expect(
      Util.fcForKeyLength(1, "abcdefghijklmnop      qrstuvwxyz")
    ).toBeCloseTo(0.0);
    expect(Util.fcForKeyLength(0, "abcdefghijklmnopqrstuvwxyz")).toBeCloseTo(
      0.0
    );
  });
  test("Empty texts should always return 0", () => {
    expect(Util.fcForKeyLength(1, "")).toBeCloseTo(0.0);
    expect(Util.fcForKeyLength(-1, "")).toBeCloseTo(0.0);
    expect(Util.friedmansCharacteristic("")).toBeCloseTo(0.0);
  });
  test("Negative keylength should throw an error", () => {
    expect(() => {
      Util.fcForKeyLength(-1, "a");
    }).toThrow();
  });
});
