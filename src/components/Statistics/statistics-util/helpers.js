import { relativeFrequencyAll, statRelativeFrequency } from "./statistics-util";

/**
 * Updated an array of objects: If the prop value of a target array element is the same as
 * the prop value of a source array element, update the target element to the source element.
 * Otherwise, simply include the entire source element
 * @param {Array} target
 * @param {Array} source
 * @param {String} prop
 * @returns
 */
export function mergeByProperty(target, source, prop = "letter") {
  source.forEach((sourceElement) => {
    const targetElement = target.find((elem) => {
      return sourceElement[prop] === elem[prop];
    });
    if (targetElement) {
      cleanUnusedElements(sourceElement, targetElement);
      Object.assign(targetElement, sourceElement);
    } else {
      target.push(sourceElement);
    }
  });
  return target;
}

function cleanUnusedElements(sourceElement, targetElement) {
  if ("Rel. Häufigkeit G0" in sourceElement) {
    /* eslint no-param-reassign: "off" */
    delete targetElement["Rel. Häufigkeit"];

    const maxInt = Object.keys(sourceElement).reduce((max, curr) => {
      const curInt = parseInt(curr.slice(-1), 10);
      if (/Rel. Häufigkeit G[0-9]/.test(curr) && curInt > max) {
        return curInt;
      }
      return max;
    }, 0);

    Object.keys(targetElement)
      .filter((el) => /Rel. Häufigkeit G[0-9]/.test(el))
      .forEach((key) => {
        if (parseInt(key.slice(-1), 10) > maxInt) {
          delete targetElement[key];
        }
      });
  } else if ("Rel. Häufigkeit" in sourceElement) {
    const propsToDelete = Object.keys(targetElement).filter((key) =>
      /Rel. Häufigkeit G[0-9]/.test(key)
    );
    propsToDelete.forEach((key) => {
      delete targetElement[key];
    });
  }
}

export function getLangStatsWithRenamedFields(language) {
  try {
    return statRelativeFrequency(language).map((elem) => {
      const suffix = language === "default" ? "" : ` ${language}`;
      return renameField(elem, "freq", `Rel. Häufigkeit${suffix}`);
    });
  } catch (e) {
    return [];
  }
}

export function getRelFreqWithRenamedFields(inputText, keyLength) {
  if (parseInt(keyLength, 10) === 0 || parseInt(keyLength, 10) === 1) {
    return relativeFrequencyAll(inputText, 1).map((elem) => {
      return renameField(elem, "freq_0", "Rel. Häufigkeit");
    });
  }
  return relativeFrequencyAll(inputText, keyLength).map((elem) => {
    for (let i = 0; i < keyLength; i += 1) {
      elem = renameField(elem, `freq_${i}`, `Rel. Häufigkeit G${i}`);
    }
    return elem;
  });
}

function renameField(elem, from, to) {
  elem[to] = elem[from];
  delete elem[from];
  return elem;
}
