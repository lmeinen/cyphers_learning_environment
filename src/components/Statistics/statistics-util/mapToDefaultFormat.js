import { data } from "./language-frequencies";

export function mapToDefaultFormat(language) {
  const freqs = data[language];
  const total = freqs.reduce((curr, el) => {
    if (/[a-zA-Z]/.test(el.letter)) {
      return curr + el.freq;
    }
    return curr;
  }, 0.0);
  const mapped = freqs
    .filter((el) => /[a-zA-Z]/.test(el.letter))
    .map((el) => {
      return { letter: el.letter.toLowerCase(), freq: el.freq / total };
    })
    .sort((a, b) => {
      if (a.letter > b.letter) {
        return 1;
      }
      if (b.letter > a.letter) {
        return -1;
      }
      return 0;
    });
  if (mapped.length !== 26) {
    throw new Error("wrong length:\n\t%o", mapped);
  }
  // eslint-disable-next-line no-console
  console.log(mapped);
}
