import React from "react";
import PropTypes from "prop-types";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
} from "recharts";

import { fcForKeyLength } from "../statistics-util/statistics-util";

function FCChart({ statText, keyLength, colours }) {
  let fcVal = 0.0;
  try {
    fcVal = fcForKeyLength(keyLength, statText);
  } catch (e) {
    fcVal = 0.0;
  }
  const fcData = [{ fc: fcVal }];

  return (
    <ResponsiveContainer width="100%" height="100%">
      <BarChart
        data={fcData}
        layout="vertical"
        margin={{
          top: 0,
          right: 5,
          bottom: 5,
          left: 5,
        }}
      >
        <Tooltip
          formatter={(value) => value.toFixed(4)}
          labelFormatter={() => ""}
          allowEscapeViewBox={{ x: true, y: false }}
          offset={30}
          animationDuration={500}
        />
        <XAxis
          type="number"
          padding={{ top: 0 }}
          height={15}
          tickSize={3}
          tickMargin={3}
          domain={[0.0, 0.1]}
          scale="linear"
        />
        <YAxis type="category" padding={{ bottom: -10 }} hide />
        <Bar
          dataKey="fc"
          minPointSize={3}
          name="Friedmansche Charakteristik"
          fill={colours[0]}
        />
      </BarChart>
    </ResponsiveContainer>
  );
}

FCChart.propTypes = {
  statText: PropTypes.string.isRequired,
  keyLength: PropTypes.number.isRequired,
  colours: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default FCChart;
