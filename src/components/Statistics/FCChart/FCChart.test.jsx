import React from "react";
import { render } from "@testing-library/react";
import FCChart from "./FCChart";

describe("FCChart component", () => {
  test("Basic FCChart render", () => {
    render(<FCChart statText="hello" keyLength={1} colours={["#FFFFFF"]} />);
  });
  test("Shouldn't crash for empty string or invalid keylengths", () => {
    render(<FCChart statText="" keyLength={1} colours={["#FFFFFF"]} />);
    render(<FCChart statText="hello" keyLength={0} colours={["#FFFFFF"]} />);
    render(<FCChart statText="hello" keyLength={-1} colours={["#FFFFFF"]} />);
  });
});
