import React from "react";
import { statRelativeFrequency } from "../Statistics/statistics-util/statistics-util";

function Theory() {
  const fcDE = fcForRelFreqs(statRelativeFrequency("DE"));
  const fcFR = fcForRelFreqs(statRelativeFrequency("FR"));
  const fcEN = fcForRelFreqs(statRelativeFrequency("EN"));
  return (
    <div>
      <p>TODO:</p>
      <ul>
        <li>Übersicht der statischen Tools</li>
      </ul>
      <p>FC for languages:</p>
      <ul>
        <li>DE: {fcDE}</li>
        <li>FR: {fcFR}</li>
        <li>EN: {fcEN}</li>
      </ul>
    </div>
  );
}

function fcForRelFreqs(relFreqs) {
  const mappedRelativeFrequencies = relFreqs.map(
    (el) => (el.freq - 1 / 26) ** 2
  );
  const fc = mappedRelativeFrequencies.reduce((a, b) => {
    return a + b;
  }, 0.0);
  return fc;
}

export default Theory;
